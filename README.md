# README #

### What is this repository for? ###

* vim plugin and syntax highlight for smali files.
* copied and modifed from [here](http://www.claudxiao.net/2012/07/adding-smali-syntax-for-vim-and-ctags/)

### How do I get set up? ###
##vim plugin
* copy vim/plugin/taglist.vim to .vim/plugin 
* copy vim/syntax/smali.vim to .vim/syntax/
* copy filetype.vim to .vim/
* make sure enable filetype in .vimrc
`filetype on`
`filetype plugin on`

##ctags
* copy or concanate ctags to ~/.ctags
* if you didn't install exuberant ctags, install it `$brew install ctags` on Mac OSX or `apt-get install exuberant-ctags` on Ubuntu
* enable tablist in vim
`:TlistToggle`